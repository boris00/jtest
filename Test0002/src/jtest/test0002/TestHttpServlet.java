/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtest.test0002;

/*
 * 1) link Libc.jar into
 *		tomcat8-examples/examples/WEB-INF/lib/jtest-libc.jar
 * 2) cp jtest/test0002/TestHttpServlet.class into
 *		tomcat8-examples/examples/WEB-INF/classes/jtest/test0002/
 * 3) add the next into tomcat8-examples/examples/WEB-INF/web.xml
 *	<servlet>
 *+		<servlet-name>JtestTestHttpServlet</servlet-name>
 *+		<servlet-class>jtest.test0002.TestHttpServlet</servlet-class>
 *	</servlet>
 *	<servlet-mapping>
 *+		<servlet-name>JtestTestHttpServlet</servlet-name>
 *+		<url-pattern>/servlets/jtest/testhttpservlet</url-pattern>
 *	</servlet-mapping>
 * 4) check http://<host>:8080/examples/servlets/jtest/testhttpservlet
 * 5) copy testhttpservlet.html into
 *		tomcat8-examples/examples/servlets/jtest/
 * 6) check http://<host>:8080/examples/servlets/jtest/testhttpservlet.html
 *
 */


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author b00
 */
public class TestHttpServlet extends HttpServlet {
	int count;
	public void init(ServletConfig sc) throws ServletException {
		super.init(sc);
		log("init");
		count = 0;
	}
	void do_params(HttpServletRequest req, HttpServletResponse resp,
			String type)
			throws ServletException, IOException {
		String colour;
		colour = req.getParameter("colour");
		resp.setContentType("text/html");
		try (PrintWriter pw = resp.getWriter()) {
			pw.printf(String.format("TestHttpServlet: %s, count %d\n",
				type, count));
			if (colour != null) {
				log(String.format("colour %s\n", colour));
				pw.printf(String.format("<p>colour %s</p>\n",
					colour));
			}
		}
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		count += 1;
		log(String.format("post %d", count));
		do_params(req, resp, "POST");
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		count += 1;
		log(String.format("get %d", count));
		do_params(req, resp, "GET");
	}
}
