/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtest.test0002;

/*
 * 1) link Libc.jar into
 *		tomcat8-examples/examples/WEB-INF/lib/jtest-libc.jar
 * 2) cp jtest/test0002/TestServlet.class into
 *		tomcat8-examples/examples/WEB-INF/classes/jtest/test0002/
 * 3) add the next into tomcat8-examples/examples/WEB-INF/web.xml
 *	<servlet>
 *+		<servlet-name>JtestTestServlet</servlet-name>
 *+		<servlet-class>jtest.test0002.TestServlet</servlet-class>
 *	</servlet>
 *	<servlet-mapping>
 *+		<servlet-name>JtestTestServlet</servlet-name>
 *+		<url-pattern>/servlets/jtest/testservlet</url-pattern>
 *	</servlet-mapping>
 * 4) check http://<host>:8080/examples/servlets/jtest/testservlet
 * 5) copy testservlet.html into
 *		tomcat8-examples/examples/servlets/jtest/
 * 6) check http://<host>:8080/examples/servlets/jtest/testservlet.html
 *
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import static jtest.libc.Csyslog.LOG_DEBUG;
import static jtest.libc.Csyslog.syslog;

/**
 * A class to check the GenericServlet
 * @author b00
 */
public class TestServlet extends GenericServlet {
	/**
	 * An usage counter of an instance of the servlet
	 */
	int count;
	/**
	 * The method to install a servlet in a container
	 * <p>To see {@link javax.servlet.GenericServlet#init}</p>
	 * @param sc a servlet configuration
	 * @throws ServletException
	 */
	public void init(ServletConfig sc) throws ServletException {
		syslog(LOG_DEBUG, "TestServlet.init\n");
		super.init(sc);
		log("<host.log>TestServlet: init<no nl>");
		System.out.printf("<catalina.out>TestServlet: init\n");
		count = 0;
	}

	/**
	 * The method returns the servlet configuration that has been set
	 * on the install.
	 * <p>To see
	 * {@link jtest.test0002.TestServlet#init}
	 * </p>
	 * @return the servlet configuration
	 */
	public ServletConfig getServletConfig() {
		return super.getServletConfig();
	}
	/**
	 * The method to process a request and to form a response
	 * <p>This implementation shows the counter of the usage.
	 * Also, it shows http parameters that have been passed in the sr.</p>
	 * <p>To see {@link javax.servlet.GenericServlet#service}</p>
	 * @param sr a request
	 * @param sr1 a response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void service(ServletRequest sr, ServletResponse sr1)
			throws ServletException, IOException {
		count += 1;
		syslog(LOG_DEBUG, "TestServlet.service: count %d\n", count);
		sr1.setContentType("text/html");
		try (PrintWriter pw = sr1.getWriter()) {
			Enumeration <String> ss;
			String s1;
			String s2;
			int np;
			pw.format("TestServlet: count %d\n", count);
			ss = sr.getParameterNames();
			np = 0;
			for (; ss.hasMoreElements(); np += 1) {
				if (np == 0)
					pw.format("<p>\n");
				s1 = ss.nextElement();
				s2 = sr.getParameter(s1);
				syslog(LOG_DEBUG, "'%s' '%s'\n", s1, s2);
				pw.format("%s: %s\n", s1, s2);
			}
			if (np != 0)
				pw.format("</p>\n");
		}
	}

	/**
	 * The method to get a descriprion of a servlet
	 * @return a text with the servlet description
	 */
	public String getServletInfo() {
		syslog(LOG_DEBUG, "TestServlet.info\n");
		return super.getServletInfo();
	}

	/**
	 * The method to uninstall the servlet
	 */
	public void destroy() {
		syslog(LOG_DEBUG, "TestServlet.destoy\n");
		super.destroy();
	}
}
