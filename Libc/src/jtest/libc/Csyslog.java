/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtest.libc;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static jtest.libc.Cerror.EINVAL;
import static jtest.libc.Cerror.EIO;

interface Csyslog_impl {
	int open();
	int close();
	void log(String s);
}

/*
 * enable syslog to listen udp 514
 * i.g., for rysslog, module(load="imudp") input(type="imudp" port="514")
 *
 */
class Csyslog_udp implements Csyslog_impl {
	static final byte [] ADDR = {127, 0, 0, 1};
	static final int PORT = 514;
	DatagramSocket sock;
	SocketAddress saddr;

	Csyslog_udp() {
		sock = null;
		saddr = null;
	}
	public int open() {
		DatagramSocket sock1;
		InetAddress addr;
		Runtime rt;
		if (sock != null)
			return -EINVAL;
		try {
			sock1 = new DatagramSocket();
		} catch (SocketException ex) {
			return -EIO;
		}
		try {
			addr = InetAddress.getByAddress(ADDR);
		} catch (UnknownHostException ex) {
			return -EINVAL;
		}
		sock = sock1;
		saddr = new InetSocketAddress(addr, PORT);
		return 0;
	}
	public int close() {
		if (sock == null)
			return 0;
		sock.close();
		sock = null;
		return 0;
	}
	public void log(String s) {
		DatagramPacket buf;
		if (sock == null)
			return;
		buf = new DatagramPacket(s.getBytes(), s.length(), saddr);
		try {
			sock.send(buf);
		} catch (IOException ex) {
		}
	}
}

class Csyslog_close extends Thread {
	public void run() {
		Csyslog.closelog();
	}
}

/**
 *
 * @author b00
 */
public class Csyslog {
	/*priorities*/
	public static final int LOG_EMERG =	0;
	public static final int LOG_ALERT =	1;
	public static final int LOG_CRIT =	2;
	public static final int LOG_ERR =	3;
	public static final int LOG_WARNING =	4;
	public static final int LOG_NOTICE =	5;
	public static final int LOG_INFO =	6;
	public static final int LOG_DEBUG =	7;
	public static final int LOG_PRIMASK =	0x07;
	/*TODO: facilities*/
	public static final int LOG_USER =	(1 << 3);
	public static final int LOG_FACMASK =	0x03f8;
	/*TODO: option*/
	public static final int LOG_PID =	0x01;
	/*private*/
	static final String TMF = "LLL dd HH:mm:ss";
	static Object lock = null;
	static Csyslog_impl impl = null;
	static String orig_name = null;
	static int connected = init();
	static String log_tag = "";
	static int log_facility = LOG_USER;
	private Csyslog() {
	}
	static int init() {
		Csyslog_impl impl1;
		Csyslog_close close1;
		Runtime rt;
		Thread cur;
		lock = new Object();
		close1 = new Csyslog_close();
		rt = Runtime.getRuntime();
		rt.addShutdownHook(close1);
		cur = Thread.currentThread();
		orig_name = cur.getName();
		impl = new Csyslog_udp();
		if (impl.open() != 0) {
			impl = null;
			return 0;
		}
		return 1;
	}
	public static void openlog(String ident, int option, int facility) {
		synchronized (lock) {
			Csyslog_impl impl1;
			if (!ident.equals(""))
				log_tag = ident;
			if (facility != 0 && (facility & ~LOG_FACMASK) == 0)
				log_facility = facility;
			if (connected == 0) {
				impl = new Csyslog_udp();
				if (impl.open() != 0)
					impl = null;
				else
					connected = 1;
			}
		}
	}
	public static void syslog(int pri, String fmt,
			Object ... argv) {
		StringBuffer buf;
		LocalDateTime tm;
		synchronized(lock) {
			if (connected == 0)
				return;
		}
		buf = new StringBuffer();
		if ((pri & LOG_FACMASK) == 0)
			pri |= log_facility;
		buf.append(String.format("<%d>", pri));
		tm = LocalDateTime.now();
		buf.append(tm.format(DateTimeFormatter.ofPattern(TMF)));
		buf.append(" ");
		buf.append(log_tag);
		/* TODO: add PID*/
		if (!log_tag.equals(""))
			buf.append(": ");
		buf.append(String.format(fmt, argv));
		synchronized(lock) {
			if (connected != 0)
				impl.log(buf.toString());
		}
	}
	public static void closelog() {
		synchronized(lock) {
			if (connected != 0) {
				impl.close();
				impl = null;
				connected = 0;
				log_tag = orig_name;
			}
		}
	}
}
