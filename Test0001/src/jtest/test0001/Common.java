/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtest.test0001;

import static jtest.libc.Cerror.EINVAL;

/**
 *
 * @author b00
 */
class Common {
	static final String V1_ADDR = "192.168.0.34";
	static final short V1_PORT = 2000;
	static int byte_addr(String addr, byte [] x) {
		String [] ss;
		int i;
		ss = addr.split("[.]", 4);
		if (ss.length != 4)
			return -EINVAL;
		i = 0;
		for (; i < 4; i += 1) {
			int v;
			try {
				v = Short.parseShort(ss[i]);
			} catch (NumberFormatException ex) {
				return -EINVAL;
			}
			if (v < 0 || v > 0xff)
				return -EINVAL;
			x[i] = (byte) v;
		}
		return 0;
	}
}
