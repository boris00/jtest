/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtest.test0001;

import static java.lang.System.exit;
import static java.lang.System.out;
import java.util.Arrays;
import static jtest.libc.Cerror.EINVAL;

/**
 *
 * @author b00
 */
public class Test0001 {
	public static int test(String [] argv) {
		String [] argv1;
		Client client;
		Server server;
		int rc = -EINVAL;
		out.printf("Test0001.java.21: ? %s\n", String.join(" ", argv));
		do {
			if (argv.length == 0)
				break;
			argv1 = Arrays.copyOfRange(argv, 1, argv.length);
			if (argv[0].equals("client")) {
				client = new Client();
				rc = client.test(argv1);
				break;
			}
			if (argv[0].equals("server")) {
				server = new Server();
				rc = server.test(argv1);
				break;
			}
		} while (false);
		out.printf("Test0001.java.33: rc %d\n", rc);
		return rc;
	}
	public static void main(String [] argv) {
		exit(test(argv));
	}
}
