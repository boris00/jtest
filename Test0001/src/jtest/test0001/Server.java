/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtest.test0001;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import static java.lang.System.exit;
import static java.lang.System.out;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicInteger;
import static jtest.libc.Cerror.EINVAL;
import static jtest.libc.Cerror.EIO;
import sun.misc.Signal;
import sun.misc.SignalHandler;

class Conn {
	static final int CBUFSIZ = 0x1000;
	Socket sock;
	InputStreamReader ir;
	char [] buf;
	Conn(Socket sock) {
		this.sock = sock;
		ir = null;
		buf = new char [CBUFSIZ];
	}
	int close() {
		while (sock != null) {
			try {
				sock.close();
				sock = null;
			} catch (IOException ex) {
			}
		}
		while (ir != null) {
			try {
				ir.close();
				ir = null;
			} catch (IOException ex) {
			}
		}
		return 0;
	}
	static void print_buf(PrintStream os, char [] buf, int n) {
		String s;
		int i;
		i = 0;
		for (; i < n; i += 1) {
			if (Character.isWhitespace(buf[i]))
				continue;
			if (!Character.isISOControl(buf[i]))
				continue;
			buf[i] = '?';
		}
		s = String.valueOf(buf, 0, n);
		os.print(s);
        }
	int process() {
		InputStream is;
		int n;
		int rc;
		if (sock == null)
			return -EINVAL;
		try {
			is = sock.getInputStream();
			sock = null;
		} catch (IOException ex) {
			return -EIO;
		}
		ir = new InputStreamReader(is);
		rc = -EIO;
		while (true) {
			try {
				n = ir.read(buf, 0, CBUFSIZ);
			} catch (IOException ex) {
				break;
			}
			if (n == -1) {
				rc = 0;
				break;
			}
			print_buf(out, buf, n);
		}
		out.printf("\n");
		close();
		return rc;
	}
}

class Server_conf {
	String		addr;
	int		port;
	Server_conf() {
		addr = Common.V1_ADDR;
		port = Common.V1_PORT;
	}
	int read(String [] argv) {
		int argc = argv.length;
		int argi = 0;
		int v;
		if (argi < argc) {
			addr = argv[argi];
			argi += 1;
		}
		if (argi < argc) {
			port = -1;
			try {
				v = Integer.parseInt(argv[argi]);
				if (v >= 0 && v <= Short.MAX_VALUE)
					port = v;
			} catch (NumberFormatException ex) {
			}
			if (port == -1)
				return -EINVAL;
			argi += 1;
		}
		if (argi != argc)
			return -EINVAL;
		return 0;
	}
}

/**
 *
 * @author b00
 */
public class Server {
	class TermHandler implements SignalHandler {
		Server server;
		TermHandler(Server server) {
			this.server = server;
		}
		public void handle(Signal signal) {
			out.printf("signal %d\n", signal.getNumber());
			server.st_run.set(0);
			try {
				server.sock.close();
			} catch (IOException ex) {
			}
		}
	}
	ServerSocket sock;
	AtomicInteger st_run;
	TermHandler term_handler;
	Server() {
		sock = null;
		st_run = new AtomicInteger(1);
		term_handler = new TermHandler(this);
	}
	void close() {
		while (sock != null) {
			try {
				sock.close();
				sock = null;
			} catch (IOException ex) {
			}
		}
	}
	int process(Socket sock) {
		InetSocketAddress addr;
		Conn conn;
		int rc;
		addr = (InetSocketAddress) sock.getRemoteSocketAddress();
		out.printf("conn %s %d\n", addr.getAddress(), addr.getPort());
		conn = new Conn(sock);
		rc = conn.process();
		conn.close();
		return rc;
	}
	int listen(Server_conf conf) {
		SocketAddress addr;
		Socket csock;
		int rc;
		rc = -EIO;
		do {
			try {
				sock = new ServerSocket();
			} catch (IOException ex) {
				break;
			}
			try {
				sock.setReuseAddress(true);
			} catch (SocketException ex) {
				break;
			}
			addr = new InetSocketAddress(conf.addr, conf.port);
			try {
				sock.bind(addr, 1);
			} catch (IOException ex) {
				break;
			}
			while (st_run.get() != 0) {
				try {
					csock = sock.accept();
				} catch (IOException ex) {
					out.printf("Server.java.135: %s\n", ex);
					continue;
				}
				process(csock);
			}
			rc = 0;
		} while (false);
		close();
		return rc;
	}
	int test(String [] argv) {
		SignalHandler sa_old;
		Signal sig;
		Server_conf conf;
		int rc;
		conf = new Server_conf();
		sig = new Signal("INT");
		sa_old = null;
		do {
			rc = conf.read(argv);
			if (rc != 0)
				break;
			sa_old = Signal.handle(sig, term_handler);
			rc = listen(conf);
			if (rc != 0)
				break;
		} while (false);
		close();
		if (sa_old != null)
			Signal.handle(sig, sa_old);
		if (rc == 0)
			out.printf("done.\n");
		return rc;
	}
	public static void main(String [] argv) {
		Server server;
		int rc;
		server = new Server();
		rc = server.test(argv);
		out.printf("rc %d\n", rc);
		exit(rc);
	}
}
