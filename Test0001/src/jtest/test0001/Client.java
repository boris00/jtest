/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtest.test0001;

import java.io.IOException;
import static java.lang.System.exit;
import static java.lang.System.out;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import static jtest.libc.Cerror.EINVAL;
import static jtest.libc.Cerror.EIO;

class Client_conf {
	String		addr;
	int		port;
	Client_conf() {
		addr = Common.V1_ADDR;
		port = Common.V1_PORT;
	}
	int read(String [] argv) {
		int argc = argv.length;
		int argi = 0;
		if (argc == 0)
			return 0;
		addr = argv[argi];
		argc -= 1;
		argi += 1;
		if (argc != 0)
			return -EINVAL;
		return 0;
	}
}

/**
 *
 * @author b00
 */
public class Client {
	Socket sock;
	Client() {
		sock = null;
	}
	int connect(Client_conf conf) {
		InetAddress addr;
		byte [] byteaddr;
		int rc;
		byteaddr = new byte [4];
		rc = Common.byte_addr(conf.addr, byteaddr);
		if (rc != 0)
			return rc;
		try {
			addr = InetAddress.getByAddress("v1", byteaddr);
		} catch (UnknownHostException ex) {
			return -EINVAL;
		}
		out.printf("connect %s %d\n", conf.addr, conf.port);
		try {
			sock = new Socket(addr, conf.port);
		} catch (IOException ex) {
			return -EIO;
		}
		return 0;
	}
	void close() {
		while (sock != null) {
			try {
				sock.close();
				sock = null;
			} catch (IOException ex) {
			}
		}
	}
	int test(String [] argv) {
		Client_conf conf;
		int rc;
		conf = new Client_conf();
		do {
			rc = conf.read(argv);
			if (rc != 0)
				break;
			rc = connect(conf);
			if (rc != 0)
				break;
			out.printf("conn\n");
		} while (false);
		close();
		if (rc == 0)
			out.printf("done.\n");
		return 0;
	}
	public static void main(String [] argv) {
		Client client;
		int rc;
		client = new Client();
		rc = client.test(argv);
		out.printf("rc %d\n", rc);
		exit(rc);
	}
}
