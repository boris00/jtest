#ifndef __COMMON_H
#define __COMMON_H

#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define V1_ADDR 	"192.168.0.34"
#define V1_PORT 	2000

#define x_pr(_fmt, ...) \
	fprintf(stderr, "%s.%d: " _fmt, __FILE__, __LINE__, ## __VA_ARGS__)

#define pr_err(_en) \
	do { \
	int en = _en; \
	fprintf(stderr, "%s.%d: error %d %s\n", \
	__FILE__, __LINE__, en, strerror(-en)); \
	} while (0);

#endif
