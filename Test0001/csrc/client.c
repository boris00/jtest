#include <stddef.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <linux/types.h>
#include "common.h"

struct conf {
	char const 	*addr;
	__s16 		port;
};

static int conf_read(struct conf *conf, int argc, char **argv)
{
	conf->addr = V1_ADDR;
	conf->port = V1_PORT;
	argc -= 1, argv += 1;
	if (!argc)
		return 0;
	conf->addr = *argv;
	argc -= 1, argv += 1;
	if (argc)
		return -EINVAL;
	return 0;
}

int main(int argc, char **argv)
{
	struct sockaddr_in addr;
	struct conf conf;
	int sock;
	int rc;

	rc = conf_read(&conf, argc, argv);
	if (rc < 0) {
		pr_err(-rc);
		goto err;
	}
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		pr_err(-errno);
		goto err;
	}
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = ntohs(conf.port);
	addr.sin_addr.s_addr = inet_addr(conf.addr);
	fprintf(stderr, "connect %s %d\n", conf.addr, conf.port);
	rc = connect(sock, (void *) &addr, sizeof(addr));
	if (rc == -1) {
		pr_err(-errno);
		goto err_sock;
	}
	fprintf(stderr, "conn\n");
	close(sock);
	fprintf(stderr, "done.\n");
	return 0;

err_sock:
	close(sock);
err:
	return -1;
}
