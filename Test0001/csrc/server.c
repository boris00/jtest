#include <stddef.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>
#include <poll.h>
#include <ctype.h>
#include <string.h>
#include <linux/types.h>
#include "common.h"

#define CBUFSIZ 0x1000

static void print_buf(FILE *fp, char *buf, int n)
{
	char *ce;
	char *c;
	char eb;
	int k;
	ce = buf + n;
	c = buf;
	for (; c < ce; c += 1) {
		if (isprint(*c))
			continue;
		if (isspace(*c))
			continue;
		*c = '?';
	}
	k = n;
	if (k == CBUFSIZ) {
		k -= 1;
		eb = buf[k];
	}
	buf[k] = '\0';
	fputs(buf, fp);
	if (k < n)
		fputc(eb, fp);
}

static volatile int run = 1;

static void term_handler(int sn)
{
	fprintf(stderr, "signal %d\n", sn);
	run = 0;
}


struct conn {
	int sock;
	char buf[CBUFSIZ + 1];
};

static int conn_process(struct conn *conn)
{
	struct pollfd fds[1];
	ssize_t n;
	int rc;
	rc = 0;
	while (run) {
		memset(fds, 0, sizeof(fds));
		fds->fd = conn->sock;
		fds->events = POLLIN;
		n = poll(fds, 1, 10000);
		if (n == -1) {
			rc = -errno;
			break;
		}
		if (!n)
			break;
		if (!(fds->events & POLLIN))
			break;
		n = recv(conn->sock, conn->buf, CBUFSIZ, 0);
		if (n  == -1) {
			rc = -errno;
			break;
		}
		if (!n)
			break;
		print_buf(stdout, conn->buf, n);
		fflush(stdout);
	}
	return rc;
}

static int process(int sock, struct sockaddr_in *addr)
{
	char abuf[INET_ADDRSTRLEN];
	struct conn *conn;
	int rc;
	inet_ntop(AF_INET, &addr->sin_addr, abuf, sizeof(abuf));
	fprintf(stderr, "conn %s %d\n", abuf, addr->sin_port);
	conn = malloc(sizeof(*conn));
	if (!conn)
		return -ENOMEM;
	conn->sock = sock;
	rc = conn_process(conn);
	close(conn->sock);
	free(conn);
	return rc;
}

struct conf {
	char const 	*addr;
	__s16 		port;
};

static int conf_read(struct conf *conf, int argc, char **argv)
{
	conf->addr = V1_ADDR;
	conf->port = V1_PORT;
	argc -= 1, argv += 1;
	if (argc) {
		conf->addr = *argv;
		argc -= 1, argv += 1;
	}
	if (argc) {
		conf->port = atoi(*argv);
		argc -= 1, argv += 1;
	}
	if (argc)
		return -EINVAL;
	return 0;
}

int main(int argc, char **argv)
{
	struct sockaddr_in addr;
	struct sockaddr_in caddr;
	struct sigaction sa;
	struct conf conf;
	int sock;
	int csock;
	int v;
	int rc;

	rc = conf_read(&conf, argc, argv);
	if (rc < 0) {
		pr_err(-rc);
		goto err;
	}
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = term_handler;
	rc = sigaction(SIGINT, &sa, NULL);
	if (rc == -1) {
		pr_err(-errno);
		goto err;
	}
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		pr_err(-errno);
		goto err;
	}
	v = 1;
	rc = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(int));
	if (rc == -1) {
		pr_err(-errno);
		goto err_sock;
	}
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(conf.port);
	addr.sin_addr.s_addr = inet_addr(conf.addr);
	rc = bind(sock, (void *) &addr, sizeof(addr));
	if (rc) {
		pr_err(-errno);
		goto err_sock;
	}
	rc = listen(sock, 1);
	if (rc) {
		pr_err(-errno);
		goto err_sock;
	}
	fprintf(stderr, "listen %s %d\n", conf.addr, conf.port);
	while (run) {
		int alen = sizeof(caddr);
		csock = accept(sock, (void *) &caddr, &alen);
		if (csock == -1) {
			pr_err(-errno);
			continue;
		}
		rc = process(csock, &caddr);
		if (rc < 0) {
			pr_err(rc);
			close(csock);
		}
	}
	do {
		rc = close(sock);
	} while (rc == -1 && errno == EINTR);
	if (rc == -1) {
		pr_err(-errno);
		goto err;
	}
	fprintf(stderr, "done.\n");
	return 0;

err_sock:
	do {
		rc = close(sock);
	} while (rc == -1 && errno == EINTR);
	if (rc == -1)
		pr_err(-errno);
err:
	return -1;
}

